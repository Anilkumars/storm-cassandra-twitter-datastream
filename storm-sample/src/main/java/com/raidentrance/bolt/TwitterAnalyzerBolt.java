package com.raidentrance.bolt;

 

import java.io.IOException;

 

import java.net.InetSocketAddress;
import java.util.Map;
import com.datastax.oss.driver.api.core.CqlSession;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;
import com.raidentrance.util.FileManager;
import com.raidentrance.util.Languages;

 

import twitter4j.Status;

 


public class TwitterAnalyzerBolt extends BaseRichBolt {
    
    
        
        
    private FileManager manager = new FileManager();
    private OutputCollector collector;
    protected transient CqlSession _session;
    
    private static final long serialVersionUID = 8465078768241865446L;
    long twitter_id = 0;
    String twitter_tweets = "";
    String User_id ="";
    @Override
    public void prepare(@SuppressWarnings("rawtypes") Map stormConf, TopologyContext context,
            OutputCollector collector) {
        CqlSession session = CqlSession.builder()
                .addContactPoint(new InetSocketAddress("34.72.144.196", 9042))
                .withLocalDatacenter("datacenter1")
                .withKeyspace("demo")
                .build();
     try {
		session.wait(10000000);
	} catch (InterruptedException e) {
		
		e.printStackTrace();
	}
        
        this.collector = collector;
        this._session = session;
    }
    
    
    @Override
    public void execute(Tuple tuple){
        
        final Status tweet = (Status) tuple.getValueByField("status");
    
        
        for (Languages language : Languages.values()) {
            if (tweet.getText().toLowerCase().contains(language.getName())) {
                try {
                    manager.writeTweet(tweet, language.getFileName());
                    
                } catch (IOException e) {
                    collector.fail(tuple);
                }
            }
        }
        
        twitter_id = tweet.getId();
        twitter_tweets = tweet.getText();
        User_id = tweet.getUser().getName();
        String t = "'" + twitter_tweets + "'" ;
        String f ="'" + User_id + "'" ;
        
        String tt =String.format("insert into demo.Twitter_table( twitter_id , twitter_tweets , user_id) values (%07d , %s, %s)",twitter_id,t,f); 
        _session.execute(tt );      
        
        System.out.println("\n" + tweet.getText() + "\n Retweet count : " + tweet.getUser().getFollowersCount()
                + "\n Tweet id " + twitter_id + "\n User id" + tweet.getUser().getName()
                + "\n hastag " + tweet.getHashtagEntities()
                + "\n----------------------------------------");
    }
    
    
             
    

 

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    }
    

 

}
