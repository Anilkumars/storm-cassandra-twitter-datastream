/**
 * 
 */
package com.raidentrance.util;

/**
 * @author alex @raidentrance
 *
 */
public enum Languages {
	 Airtel("AIRTEl","AIRTEL.txt");

	private String name;
	private String fileName;

	private Languages(String name, String fileName) {
		this.name = name;
		this.fileName = fileName;
	}

	public String getName() {
		return name;
	}

	public String getFileName() {
		return fileName;
	}

}
